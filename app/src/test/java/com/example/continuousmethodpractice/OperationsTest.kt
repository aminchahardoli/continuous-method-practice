package com.example.continuousmethodpractice

import org.junit.Test
import org.junit.Assert.*

class OperationsTest {

    @Test
    fun sumIsCorrect(){
        val operations = Operations()
        val result = operations.sum(10, 20)
        assertEquals(30, result)
    }
}